![hero image](docs/images/jscad-hardware.png)

[![view on npm](http://img.shields.io/npm/v/@jwc/jscad-hardware.svg)](https://www.npmjs.org/package/@jwc/jscad-hardware)
[![npm module downloads](http://img.shields.io/npm/dt/@jwc/jscad-hardware.svg)](https://www.npmjs.org/package/@jwc/jscad-hardware)
![Dependents](http://img.shields.io/librariesio/dependents/npm/@jwc/jscad-hardware)

![Twitter Follow](https://img.shields.io/twitter/follow/johnwebbcole?label=Follow&style=social)

# jscad-hardware

> https://gitlab.com/johnwebbcole/jscad-hardware

This is a collection of jscad parts that model a common Imperial and Metric bolts and washers. These models use the [jscad-utils](https://johnwebbcole.gitlab.io/jscad-utiddef) library and return jscad-utils [`group` objects](https://johnwebbcole.gitlab.io/jscad-utils/code/group.html).

## Installation

Install `@jwc/jscad-hardware` using NPM:

```bash
npm install --save @jwc/jscad-hardware
```

## Basic usage

To use the utilities, you need to include the `jscad-hardware.jscad`.

```javascript
include('node_modules/jscad-utils/jscad-utils.jscad');
include('node_modules/jscad-hardware/jscad-hardware.jscad');

main() {
  util.init(CSG);

  var bolt = Hardware.Bolt(
    util.inch(1),
    ImperialBolts['5/16 hex'],
    'close'
  )

  return bolt.combine('head,thread');
}
```

## Yeoman Generator

You can use the [yeoman jscad generator](https://www.npmjs.com/package/generator-jscad) which will create a project that uses this library.

Once you create your project, install `jscad-hardware`, and run `gulp`. Dragging the `dist` directory into [http://openjscad.org/](http://openjscad.org/) will include this library.

Modify your `main.jscad` file to return a bolt object.

```javascript
function main() {
  util.init(CSG);

  var bolt = Hardware.Bolt(util.inch(1), ImperialBolts["5/16 hex"], "close");

  return bolt.combine("head,thread");
}

// include:js
// endinject
```

![bplus example](docs/images/bolt.png)
