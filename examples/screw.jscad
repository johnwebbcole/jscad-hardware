var BOM = {};

function main() {
  util.init(CSG, { debug: "*" });
  var screw = Hardware.Screw.FlatHead(
    ImperialWoodScrews["#8"],
    util.inch(3 / 8),
    "loose",
    { name: "screw1" }
  );
  return [screw.combine()];
}

// include:js
// endinject
