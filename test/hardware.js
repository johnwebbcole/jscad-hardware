import test from 'ava';
// import { nearlyEqual } from './helpers/nearlyEqual';
import * as Hardware from '../src/hardware';
import {
  ImperialBolts,
  ImperialWoodScrews,
  ImperialNuts,
  ImperialWashers,
  RSeriesBearings
} from '../src/imperial';
import csgImageSnapshot from './helpers/csgImageSnapshot';

// import { util, init as utilInit } from '@jwc/jscad-utils';
// console.warn('utilInit', utilInit.default);

test('import util', t => {
  // console.log(
  //     Object.keys(util)
  //         .map(k => `test.todo('${k}');`)
  //         .join('\n')
  // );

  t.snapshot(Object.keys(Hardware).sort());
});

test('imperial bolt', async t => {
  var bolt = Hardware.Bolt(util.inch(1), ImperialBolts['1/4 hex'], 'close');
  const value = await csgImageSnapshot(t, bolt.combine().color('blue'));
  t.true(value);
});

test.todo('Clearances');
test.todo('CreateScrew');

test('imperial FlatHeadScrew', async t => {
  var h = Hardware.Screw.FlatHead(
    ImperialWoodScrews['#10'],
    util.inch(1),
    'loose',
    { name: 'screw1' }
  );
  const value = await csgImageSnapshot(t, h.combine().color('blue'));
  t.true(value);
});

test('imperial HexHeadScrew', async t => {
  var h = Hardware.HexHeadScrew(
    ...ImperialWoodScrews['#10'].slice(0, 3),
    util.inch(1)
  );
  const value = await csgImageSnapshot(t, h.combine().color('blue'));
  t.true(value);
});

test('imperial Nut', async t => {
  var h = Hardware.Nut(ImperialNuts['#10 hex']);
  const value = await csgImageSnapshot(t, h.color('blue'));
  t.true(value);
});

test.todo('Orientation');
test.todo('imperial PanHeadScrew');
test.todo('imperial Screw');
test('imperial Washer', async t => {
  var h = Hardware.Washer(ImperialWashers['1/4']);
  const value = await csgImageSnapshot(t, h.combineAll());
  t.true(value);
});

test('imperial Bearing', async t => {
  var h = Hardware.Bearing(RSeriesBearings['R4']);
  const value = await csgImageSnapshot(t, h.combine().color('blue'));
  t.true(value);
});
