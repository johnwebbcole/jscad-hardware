import test from 'ava';
// import { nearlyEqual } from './helpers/nearlyEqual';
// import * as Hardware from '../src/hardware';
import * as metric from '../src/metric';

function object2table(o, header = [], keys) {
  var padding = 12;
  var includeKeys = keys || Object.keys(Object.values(o)[0]);
  /**
   * use the header param or build the headers based on the
   * keys from the first set of values.
   */
  var rows =
    header.length > 0
      ? [header.map(x => x.padEnd(padding, ' ')).join(' ')]
      : ['name      ' + includeKeys.map(x => x.padEnd(padding, ' ')).join(' ')];

  /**
   * For each entry, create a new array of values with the key as the first
   * entry.
   */

  for (let [key, value] of Object.entries(o)) {
    var line = [
      key,
      ...Object.values(includeKeys)
        .map(y => value[y] || '')
        .map(x => (typeof x == 'number' ? x.toFixed(4) : x))
    ];
    rows.push(line.map(x => x.padEnd(padding, ' ')).join(' '));
  }
  return rows.join('\n');
}

test('import util', t => {
  t.snapshot(Object.keys(metric).sort());
});

test('metricBolts', t => {
  t.snapshot(
    object2table(
      metric.MetricBolts,
      [
        'name',
        'tap',
        'close',
        'loose',
        'Body Diam (E)',
        'Face Wth (F)',
        'Corner Wth (G)',
        'Head Hgt (H)',
        'type'
      ],
      ['tap', 'close', 'loose', 'E', 'F', 'G', 'H', 'type']
    )
  );
});

test('metricNuts', t => {
  t.snapshot(
    object2table(
      metric.MetricNuts,
      ['name', 'Face Wth (F)', 'Point W (C)', 'Head Hgt (H)', 'Diam (D)'],
      [0, 1, 2, 3]
    )
  );
});

test('metricWashers', t => {
  t.snapshot(
    object2table(
      metric.MetricWashers,
      ['size', 'id', 'od', 'thickness'],
      ['id', 'od', 'thickness']
    )
  );
});

test('metricScrews', t => {
  t.snapshot(
    object2table(metric.MetricScrews, [
      'name',
      'headDiameter',
      'headLength',
      'diameter'
    ])
  );
});
