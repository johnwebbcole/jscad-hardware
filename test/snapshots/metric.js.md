# Snapshot report for `test/metric.js`

The actual snapshot is saved in `metric.js.snap`.

Generated by [AVA](https://avajs.dev).

## import util

> Snapshot 1

    [
      'MetricBolts',
      'MetricFlatHeadScrews',
      'MetricNuts',
      'MetricScrews',
      'MetricWashers',
    ]

## metricBolts

> Snapshot 1

    `name         tap          close        loose        Body Diam (E) Face Wth (F) Corner Wth (G) Head Hgt (H) type        ␊
    m10 hex      8.8000       10.5000      11.0000      10.0000      17.0000      18.4800      6.6300       HexHeadScrew␊
    m12 hex      10.5000      12.6000      13.2000      12.0000      19.0000      20.7800      7.7600       HexHeadScrew␊
    m14 hex      12.5000      14.7500      15.5000      14.0000      22.0000      24.2500      9.0900       HexHeadScrew␊
    m16 hex      14.0000      16.7500      17.5000      16.0000      24.0000      27.7100      10.3200      HexHeadScrew␊
    m20 hex      18.0000      21.0000      22.0000      20.0000      30.0000      34.6400      12.8800      HexHeadScrew␊
    m24 hex      24.0000      24.0000      24.0000      24.0000      36.0000      41.5700      15.4400      HexHeadScrew␊
    m30 hex      30.0000      30.0000      30.0000      30.0000      46.0000      53.1200      19.4800      HexHeadScrew␊
    m36 hex      36.0000      36.0000      36.0000      36.0000      55.0000      63.5100      23.3800      HexHeadScrew␊
    m42 hex      42.0000      42.0000      42.0000      42.0000      65.0000      75.0600      26.9700      HexHeadScrew␊
    m48 hex      48.0000      48.0000      48.0000      48.0000      75.0000      86.6000      31.0700      HexHeadScrew␊
    m56 hex      56.0000      56.0000      56.0000      56.0000      85.0000      98.1500      36.2000      HexHeadScrew␊
    m64 hex      64.0000      64.0000      64.0000      64.0000      95.0000      109.7000     41.3200      HexHeadScrew␊
    m72 hex      72.0000      72.0000      72.0000      72.0000      105.0000     121.2400     46.4500      HexHeadScrew␊
    m80 hex      80.0000      80.0000      80.0000      80.0000      115.0000     132.7200     51.5800      HexHeadScrew␊
    m90 hex      90.0000      90.0000      90.0000      90.0000      130.0000     150.1100     57.7400      HexHeadScrew␊
    m100 hex     90.0000      90.0000      90.0000      90.0000      145.0000     167.4300     63.9000      HexHeadScrew␊
    m1.6 socket  1.2500       1.7000       1.7500       1.6000                                 1.6000       PanHeadScrew␊
    m2 socket    1.5500       2.1000       2.2000       2.0000                                 2.0000       PanHeadScrew␊
    m2.5 socket  2.0500       2.6500       2.7500       2.5000                                 2.5000       PanHeadScrew␊
    m3 socket    2.4000       3.1500       3.3000       3.0000                                 3.0000       PanHeadScrew␊
    m4 socket    3.2500       4.2000       4.4000       4.0000                                 4.0000       PanHeadScrew␊
    m5 socket    4.1000       5.2500       5.5000       5.0000                                 5.0000       PanHeadScrew␊
    m6 socket    5.0000       6.3000       6.6000       6.0000                                 6.0000       PanHeadScrew␊
    m8 socket    6.8000       8.4000       8.8000       8.0000                                 8.0000       PanHeadScrew␊
    m10 socket   8.8000       10.5000      11.0000      10.0000                                10.0000      PanHeadScrew␊
    m12 socket   10.5000      12.6000      13.2000      12.0000                                12.0000      PanHeadScrew␊
    m14 socket   12.5000      14.7500      15.5000      14.0000                                14.0000      PanHeadScrew␊
    m16 socket   14.0000      16.7500      17.5000      16.0000                                16.0000      PanHeadScrew␊
    m20 socket   18.0000      21.0000      22.0000      20.0000                                20.0000      PanHeadScrew␊
    m24 socket   24.0000      24.0000      24.0000      24.0000                                24.0000      PanHeadScrew␊
    m30 socket   30.0000      30.0000      30.0000      30.0000                                30.0000      PanHeadScrew␊
    m36 socket   36.0000      36.0000      36.0000      36.0000                                36.0000      PanHeadScrew␊
    m42 socket   42.0000      42.0000      42.0000      42.0000                                42.0000      PanHeadScrew`

## metricNuts

> Snapshot 1

    `name         Face Wth (F) Point W (C)  Head Hgt (H) Diam (D)    ␊
    m4           7.0000       8.1000       3.2000       4.0000      `

## metricScrews

> Snapshot 1

    `name         headDiameter headLength   diameter    ␊
    m4           8.0000       3.1000       4.0000      `

## metricWashers

> Snapshot 1

    `size         id           od           thickness   ␊
    M1           1.1000       3.2000       0.3000      ␊
    M1.2         1.3000       3.8000       0.3000      ␊
    M1.4         1.5000       3.8000       0.3000      ␊
    M1.6         1.7000       4.0000       0.3000      ␊
    M2           2.2000       5.0000       0.3000      ␊
    M2.5         2.7000       6.0000       0.5000      ␊
    M3           3.2000       7.0000       0.5000      ␊
    M3.5         3.7000       8.0000       0.5000      ␊
    M4           4.3000       9.0000       0.8000      ␊
    M5           5.3000       10.0000      1.0000      ␊
    M6           6.4000       12.0000      1.6000      ␊
    M7           7.4000       14.0000      1.6000      ␊
    M8           8.4000       16.0000      1.6000      ␊
    M10          10.5000      20.0000      2.0000      ␊
    M11          12.0000      24.0000      2.5000      ␊
    M12          13.0000      24.0000      2.5000      ␊
    M14          15.0000      28.0000      2.5000      ␊
    M16          17.0000      30.0000      3.0000      ␊
    M18          19.0000      34.0000      3.0000      ␊
    M20          21.0000      37.0000      3.0000      `
