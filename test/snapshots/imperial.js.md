# Snapshot report for `test/imperial.js`

The actual snapshot is saved in `imperial.js.snap`.

Generated by [AVA](https://avajs.dev).

## ImperialBolts

> Snapshot 1

    `name         tap          close        loose        Body Diam (E) Face Wth (F) Corner Wth (G) Head Hgt (H) type        ␊
    1/4 hex      5.1054       6.5278       6.7564       6.3500       11.1125      12.8270      3.9688       HexHeadScrew␊
    5/16 hex     6.5278       8.2042       8.4328       7.9375       12.7000      14.6558      5.1594       HexHeadScrew␊
    3/8 hex      7.9375       9.8044       10.0838      9.5250       14.2875      16.5100      5.9531       HexHeadScrew␊
    7/16 hex     9.3472       11.5087      11.9050      11.1125      15.8750      18.3388      7.1437       HexHeadScrew␊
    1/2 hex      10.7163      13.0962      13.4925      12.7000      19.0500      21.9964      7.9375       HexHeadScrew␊
    9/16 hex     12.3038      14.6837      15.0825      14.2875      20.6375      23.8252      9.1281       HexHeadScrew␊
    5/8 hex      13.4925      16.2712      16.6675      15.8750      23.8125      27.5082      9.9219       HexHeadScrew␊
    3/4 hex      16.6675      19.4462      19.8425      19.0500      28.5750      32.9946      11.9063      HexHeadScrew␊
    7/8 hex      19.4462      22.6212      23.0175      22.2250      33.3375      38.5064      14.6844      HexHeadScrew␊
    1 hex        22.2250      25.7962      26.1950      25.4000      38.1000      43.9928      17.0656      HexHeadScrew␊
    1-1/4 hex    31.7500      31.7500      31.7500      31.7500      47.6250      54.9910      21.4312      HexHeadScrew␊
    #0 socket    2.4384       2.4384       2.4384       1.5240                                 1.5240       PanHeadScrew␊
    #1 socket    22.2250      25.7962      26.1950      1.8542                                 1.8542       PanHeadScrew␊
    #2 socket    3.5560       3.5560       3.5560       2.1844                                 2.1844       PanHeadScrew␊
    #3 socket    4.0894       4.0894       4.0894       2.5146                                 2.5146       PanHeadScrew␊
    #4 socket    4.6482       4.6482       4.6482       2.8448                                 2.8448       PanHeadScrew␊
    #5 socket    5.2070       5.2070       5.2070       3.1750                                 3.1750       PanHeadScrew␊
    #6 socket    5.7404       5.7404       5.7404       3.5052                                 3.5052       PanHeadScrew␊
    #8 socket    6.8580       6.8580       6.8580       4.1656                                 4.1656       PanHeadScrew␊
    #10 socket   7.9248       7.9248       7.9248       4.8260                                 5.2578       PanHeadScrew␊
    1/4 socket   5.1054       6.5278       6.7564       6.3500                                 6.3500       PanHeadScrew␊
    5/16 socket  6.5278       8.2042       8.4328       7.9375                                 7.9248       PanHeadScrew␊
    3/8 socket   7.9375       9.8044       10.0838      9.5250                                 9.5250       PanHeadScrew␊
    7/16 socket  9.3472       11.5087      11.9050      11.1125                                11.1252      PanHeadScrew␊
    1/2 socket   10.7163      13.0962      13.4925      12.7000                                12.7000      PanHeadScrew␊
    5/8 socket   13.4925      16.2712      16.6675      15.8750                                15.8750      PanHeadScrew␊
    3/4 socket   16.6675      19.4462      19.8425      19.0500                                19.0500      PanHeadScrew␊
    7/8 socket   19.4462      22.6212      23.0175      22.2250                                22.2250      PanHeadScrew␊
    1 socket     22.2250      25.7962      26.1950      25.4000                                25.4000      PanHeadScrew␊
    1-1/4 socket 47.6250      47.6250      47.6250      31.7500                                31.7500      PanHeadScrew␊
    1-1/2 socket 57.1500      57.1500      57.1500      38.1000                                38.1000      PanHeadScrew`

## ImperialNuts

> Snapshot 1

    `name         Face Wth (F) Point W (C)  Head Hgt (H) Diam (D)    ␊
    3/8 lock     14.2875      16.4978      5.5800       8.2489      ␊
    #0 hex       3.9688       4.5827       1.1906       2.2914      ␊
    #1 hex       3.9688       4.5827       1.1906       2.2914      ␊
    #2 hex       4.7625       5.4993       1.5875       2.7496      ␊
    #3 hex       4.7625       5.4993       1.5875       2.7496      ␊
    #4 hex       6.3500       7.3323       2.3812       3.6662      ␊
    #6 hex       7.9375       9.1654       2.7781       4.5827      ␊
    #8 hex       8.7312       10.0820      3.1750       5.0410      ␊
    #10 hex      9.5250       10.9985      3.1750       5.4993      ␊
    #12 hex      11.1125      12.8316      3.9688       6.4158      ␊
    1/4 hex      11.1125      12.8316      5.5562       6.4158      ␊
    5/16 hex     12.7000      14.6647      6.7469       7.3323      ␊
    3/8 hex      14.2875      16.4978      8.3344       8.2489      ␊
    7/16 hex     17.4625      20.1640      9.5250       10.0820     ␊
    1/2 hex      19.0500      21.9970      11.1125      10.9985     ␊
    9/16 hex     22.2250      25.6632      12.3031      12.8316     ␊
    5/8 hex      23.8125      27.4963      13.8906      13.7482     ␊
    3/4 hex      28.5750      32.9956      16.2719      16.4978     ␊
    7/8 hex      33.3375      38.4948      19.0500      19.2474     ␊
    1 hex        38.1000      43.9941      21.8281      21.9970     `

## ImperialWashers

> Snapshot 1

    `size         id           od           thickness   ␊
    1            26.9875      63.5000      4.3656      ␊
    2            53.9750      114.3000     4.7625      ␊
    3            79.3750      139.7000     7.1437      ␊
    3/16         6.3500       14.2875      1.1906      ␊
    1/4          7.9375       19.0500      1.5875      ␊
    5/16         9.5250       22.2250      1.9844      ␊
    3/8          11.1125      25.4000      1.9844      ␊
    7/16         12.7000      31.7500      1.9844      ␊
    1/2          14.2875      34.9250      2.7781      ␊
    9/16         15.8750      38.1000      2.7781      ␊
    5/8          17.4625      44.4500      3.5719      ␊
    3/4          20.6375      50.8000      3.9688      ␊
    7/8          23.8125      57.1500      0.3969      ␊
    1-1/8        31.7500      69.8500      4.3656      ␊
    1-1/4        34.9250      76.2000      4.3656      ␊
    1-3/8        38.1000      57.1500      4.7625      ␊
    1-1/2        41.2750      88.9000      4.7625      ␊
    1-5/8        44.4500      95.2500      4.7625      ␊
    1-3/4        47.6250      101.6000     4.7625      ␊
    2-1/2        66.6750      114.3000     4.7625      ␊
    1/4 fender   7.1437       31.7500      2.0320      `

## ImperialWoodScrews

> Snapshot 1

    `name         headDiameter headLength   diameter    ␊
    #2           4.3688       1.2954       2.1844                                 #2          ␊
    #3           5.0546       1.4986       2.5146                                 #3          ␊
    #4           5.7150       1.7018       2.8448                                 #4          ␊
    #5           6.4008       1.9050       3.1750                                 #5          ␊
    #6           7.0866       2.1082       3.5052                                 #6          ␊
    #7           7.7470       2.3114       3.8354                                 #7          ␊
    #8           8.4328       2.5400       4.1656                                 #8          ␊
    #9           9.0932       2.7432       4.4958                                 #9          ␊
    #10          9.7790       2.9464       4.8260                                 #10         ␊
    #12          11.1252      3.3528       5.4864                                 #12         ␊
    #14          12.8778      3.8862       6.1468                                 #14         ␊
    #16          13.8176      4.1656       6.8072                                 #16         `

## RSeriesBeaerings

> Snapshot 1

    `name         ID           OD           thickness   ␊
    R16          R16          1            2            .5000        25.4000      50.8000      12.7000     ␊
    R18          R18          1-1/8        2-1/8        .5000        28.5750      53.9750      12.7000     ␊
    R24-2RS      R24-2RS      1-1/2        2-5/8        9/16         38.1000      66.6750      228.6000    ␊
    R20          R20          1-1/4        2-1/4        .5000        31.7500      57.1500      12.7000     ␊
    R6           R6           3/8          7/8          .2812        9.5250       22.2250      7.1425      ␊
    R4A          R4A          1/4          3/4          .2812        6.3500       19.0500      7.1425      ␊
    R8           R8           1/2          1-1/8        .3125        12.7000      28.5750      7.9375      ␊
    R10          R10          5/8          1-3/8        .3438        15.8750      34.9250      8.7325      ␊
    R12          R12          3/4          1-5/8        .4375        19.0500      41.2750      11.1125     ␊
    R14          R14          7/8          1-7/8        .5000        22.2250      47.6250      12.7000     ␊
    R3           R3           3/16         1/2          .1960        4.7625       12.7000      4.9784      ␊
    R4           R4           1/4          5/8          .1960        6.3500       15.8750      4.9784      ␊
    R2           R2           1/8          3/8          .1562        3.1750       9.5250       3.9675      `

## import util

> Snapshot 1

    [
      'ImperialBolts',
      'ImperialNuts',
      'ImperialWashers',
      'ImperialWoodScrews',
      'RSeriesBearings',
    ]
