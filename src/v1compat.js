/* eslint-disable */
var Hardware,
  ImperialBolts,
  ImperialNuts,
  ImperialWashers,
  ImperialWoodScrews,
  RSeriesBearings,
  MetricBolts,
  MetricNuts,
  MetricWashers,
  MetricFlatHeadScrews;
function initJscadHardware() {
  var Debug = util.Debug;
  var debug = Debug('jscadHardware:initJscadHardware');
  var jscadUtils = { util, Debug, parts: Parts, Group };
  var jsCadCSG = { CSG, CAG };
  debug('initJscadHardware:jscadUtils', jscadUtils);

  // include:compat
  // end:compat

  debug('jscadHardware', jscadHardware);
  Hardware = jscadHardware.hardware;

  ImperialBolts = jscadHardware.imperial.ImperialBolts;
  ImperialNuts = jscadHardware.imperial.ImperialNuts;
  ImperialWashers = jscadHardware.imperial.ImperialWashers;
  ImperialWoodScrews = jscadHardware.imperial.ImperialWoodScrews;
  RSeriesBearings = jscadHardware.imperial.RSeriesBearings;

  MetricBolts = jscadHardware.metric.MetricBolts;
  MetricNuts = jscadHardware.metric.MetricNuts;
  MetricWashers = jscadHardware.metric.MetricWashers;
  MetricFlatHeadScrews = jscadHardware.metric.MetricFlatHeadScrews;
}

/**
 * Add `initJscadHardware` to the init queue for `util.init`.
 */
jscadUtilsPluginInit.push(initJscadHardware);
/* eslint-enable */
