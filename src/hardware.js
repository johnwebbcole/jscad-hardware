/* globals BOM */
import { util, Group, parts as Parts, Debug } from '@jwc/jscad-utils';
const debug = Debug('jscadHardware:hardware');
/* exported Hardware */

/**
 * Bill of material object
 */
export var BOM = {};

/**
 * Increment the bom value for a single bomkey.
 * @param {String} bomkey
 */
function BOMadd(bomkey) {
  if (!BOM[bomkey]) BOM[bomkey] = 0;
  BOM[bomkey]++;
}

/**
 * A gear moduel for openJSCAD.
 * ![parts example](jsdoc2md/hexagon.png)
 * @example
 *include('jscad-utils-color.jscad');
 *
 *function mainx(params) {
 *   util.init(CSG);
 *
 *   // draws a blue hexagon
 *   return Parts.Hexagon(10, 5).color('blue');
 *}
 * @type {Object}
 * @module jscad-hardware
 * @exports Hardware
 */
export function Bolt(length, bolt, fit) {
  fit = fit || 'loose';

  // Keep track of bolts for bill-of-materials
  // var bomkey = `Bolt ${bolt.name} - ${util.cm(length).toFixed(2)}`;
  // if (!BOM[bomkey]) BOM[bomkey] = 0;
  // BOM[bomkey]++;
  BOMadd(`Bolt ${bolt.name} - ${util.cm(length).toFixed(2)}`);

  var b = this[bolt.type](bolt.G || bolt.D, bolt.H, bolt.E, length);

  var clearance = bolt[fit] - bolt.E;

  b.add(
    this[bolt.type](
      (bolt.G || bolt.D) + clearance,
      bolt.H + clearance,
      bolt[fit],
      length,
      length
    ).map((part) => part.color('red')),
    'tap',
    false,
    'tap-'
  );

  return b;
}

/**
 * Create a washer group from a washer type.
 * @param {Object} washer Washer type object.
 * @param {String} fit    Clearance to add to group (tap|close|loose).
 */
export function Washer(washer, fit) {
  debug('Washer', washer, fit);

  // Keep track of bolts for bill-of-materials
  BOMadd(`Washer ${washer.size}`);
  // var bomkey = `Washer ${washer.size}`;
  // if (!BOM[bomkey]) BOM[bomkey] = 0;
  // BOM[bomkey]++;

  var w = Group();
  w.add(
    Parts.Tube(washer.od, washer.id, washer.thickness).color('gray'),
    'washer'
  );

  if (fit) {
    var tap = Clearances[fit];
    if (!tap)
      console.error(
        `Washer unknown fit clearance ${fit}, should be ${Object.keys(
          Clearances
        ).join('|')}`
      );
    w.add(
      Parts.Cylinder(washer.od + Clearances[fit], washer.thickness).color(
        'red'
      ),
      'clearance'
    );
  }
  return w;
}

/**
 * F is the width across the faces, C is the width
 * across the points, and H is the height.  D is the
 * basic diameter.
 * [F, C, H, D]
 * @function Nut
 * @param  {type} nut {description}
 * @param  {type} fit {description}
 * @return {type} {description}
 */
export function Nut(nut, fit = 'close') {
  if (Array.isArray(nut)) {
    var [face, corner, height, diameter, name] = nut;
    nut = { face, corner, height, diameter, name };
  }
  debug('Nut', nut, fit);
  // Keep track of nuts for bill-of-materials
  BOMadd(`Nut ${nut.name || nut.diameter}`);
  // var bomkey = `Nut ${nut[4]}`;
  // if (!BOM[bomkey]) BOM[bomkey] = 0;
  // BOM[bomkey]++;
  return Parts.Hexagon(nut.corner + Clearances[fit], nut.height);
}

export const Screw = {
  PanHead: function (type, length, fit, options = {}) {
    debug('PanHead', type, length, fit, options);
    if (Array.isArray(type)) {
      var [headDiameter, headLength, diameter, tap, countersink, name] = type;
    } else {
      var { headDiameter, headLength, diameter, name } = type;
    }

    if (!options.fit) options.fit = fit;

    // Keep track of nuts for bill-of-materials
    var bomkey = `Pan Head Screw ${name}`;
    if (!BOM[bomkey]) BOM[bomkey] = 0;
    BOM[bomkey]++;

    return PanHeadScrew(
      headDiameter,
      headLength,
      diameter,
      length,
      options.clearLength,
      options
    );
  },

  FlatHead: function (type, length, fit, options = {}) {
    debug('FlatHead', type, length, fit, options);
    if (Array.isArray(type)) {
      var [headDiameter, headLength, diameter, tap, countersink, name] = type;
    } else {
      var { headDiameter, headLength, diameter, name } = type;
    }

    if (!options.fit) options.fit = fit;

    // Keep track of nuts for bill-of-materials
    var bomkey = `Flat Head Screw ${name}`;
    if (!BOM[bomkey]) BOM[bomkey] = 0;
    BOM[bomkey]++;

    return FlatHeadScrew(
      headDiameter,
      headLength,
      diameter,
      length,
      options.clearLength,
      options
    );
  }
  // HexHead: ScrewType(PanHeadScrew, ...args)
};

export const Clearances = {
  tap: util.inch(-0.049),
  close: util.inch(0.007),
  loose: util.inch(0.016)
};

export const Orientation = {
  up: {
    head: 'outside-',
    clear: 'inside+'
  },
  down: {
    head: 'outside+',
    clear: 'inside-'
  }
};

export function CreateScrew(head, thread, headClearSpace, options = {}) {
  options = Object.assign(options, {
    orientation: 'up',
    clearance: [0, 0, 0]
  });
  var orientation = Orientation[options.orientation];
  var group = Group('head,thread', {
    head: head.color('gray'),
    thread: thread.snap(head, 'z', orientation.head).color('silver')
  });

  if (options.name) group.setName(options.name);

  if (headClearSpace) {
    group.add(
      headClearSpace
        .enlarge(options.clearance)
        .snap(head, 'z', orientation.clear)
        .color('red'),
      'headClearSpace',
      true
    );
  }

  if (options.tap) {
    group.add(
      union([head, thread]).enlarge(options.tap, options.tap, 0).color('red'),
      'tap',
      true
    );
  }

  return group;
}

/**
 * Creates a `Group` object with a Pan Head Screw.
 * @param {number} headDiameter Diameter of the head of the screw
 * @param {number} headLength   Length of the head
 * @param {number} diameter     Diameter of the threaded shaft
 * @param {number} length       Length of the threaded shaft
 * @param {number} clearLength  Length of the clearance section of the head.
 * @param {object} options      Screw options include orientation and clerance scale.
 */
export function PanHeadScrew(
  headDiameter,
  headLength,
  diameter,
  length,
  clearLength,
  options
) {
  var head = Parts.Cylinder(headDiameter, headLength);
  var thread = Parts.Cylinder(diameter, length);

  if (clearLength) {
    var headClearSpace = Parts.Cylinder(headDiameter, clearLength);
  }

  if (options.fit && !options.tap) options.tap = Clearances[options.fit];

  return CreateScrew(head, thread, headClearSpace, options);
}

/**
 * Creates a `Group` object with a Hex Head Screw.
 * @param {number} headDiameter Diameter of the head of the screw
 * @param {number} headLength   Length of the head
 * @param {number} diameter     Diameter of the threaded shaft
 * @param {number} length       Length of the threaded shaft
 * @param {number} clearLength  Length of the clearance section of the head.
 * @param {object} options      Screw options include orientation and clerance scale.
 */
export function HexHeadScrew(
  headDiameter,
  headLength,
  diameter,
  length,
  clearLength,
  options
) {
  debug(
    'HexHeadScrew',
    headDiameter,
    headLength,
    diameter,
    length,
    clearLength,
    options
  );
  var head = Parts.Hexagon(headDiameter, headLength);
  var thread = Parts.Cylinder(diameter, length);

  if (clearLength) {
    var headClearSpace = Parts.Hexagon(headDiameter, clearLength);
  }

  return CreateScrew(head, thread, headClearSpace, options);
}

/**
 * Create a Flat Head Screw
 * @param {number} headDiameter head diameter
 * @param {number} headLength   head length
 * @param {number} diameter     thread diameter
 * @param {number} length       thread length
 * @param {number} clearLength  clearance length
 * @param {object} options      options
 */
export function FlatHeadScrew(
  headDiameter,
  headLength,
  diameter,
  length,
  clearLength,
  options
) {
  var a = headDiameter;
  var b = diameter;
  if (options && options.orientation == 'down') {
    a = diameter;
    b = headDiameter;
  }
  var head = Parts.Cone(a, b, headLength);
  // var head = Parts.Cylinder(headDiameter, headLength);
  var thread = Parts.Cylinder(diameter, length);

  if (clearLength) {
    var headClearSpace = Parts.Cylinder(headDiameter, clearLength);
  }

  if (options.fit && !options.tap) options.tap = Clearances[options.fit];

  return CreateScrew(head, thread, headClearSpace, options);
}

export function Bearing({ ID, OD, thickness }, name) {
  var g = Group(name);
  debug('Bearing', ID, OD, thickness);

  var shell = Parts.Cylinder(OD, thickness).color('red');
  var core = Parts.Cylinder(ID, thickness).color('black');
  var bearing = shell.subtract(core).color('lightgray');
  bearing.properties.start = new CSG.Connector([0, 0, 0], [0, 0, 1], [0, 1, 0]);
  bearing.properties.end = new CSG.Connector(
    [0, 0, thickness],
    [0, 0, 1],
    [0, 1, 0]
  );
  g.add(bearing, 'bearing');
  g.add(shell, 'shell', true);
  g.add(core, 'core', true);

  g.properties = { ID, OD, thickness };

  return g;
}
